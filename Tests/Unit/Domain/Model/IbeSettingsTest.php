<?php
namespace Orcas\HrsIbe\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Alexander Kreetz <alexander.kreetz@orcas.de>
 */
class IbeSettingsTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Orcas\HrsIbe\Domain\Model\IbeSettings
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Orcas\HrsIbe\Domain\Model\IbeSettings();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getSettingKeyReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSettingKey()
        );
    }

    /**
     * @test
     */
    public function setSettingKeyForStringSetsSettingKey()
    {
        $this->subject->setSettingKey('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'settingKey',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getValueReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getValue()
        );
    }

    /**
     * @test
     */
    public function setValueForStringSetsValue()
    {
        $this->subject->setValue('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'value',
            $this->subject
        );
    }
}
