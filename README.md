# DS-IBE extension for TYPO3
A simple extension to use the DS IBE in your typo3 project. Provides a plugin to view the ibe in frontend 
and a backend module for configuration.

## Supported versions
- TYPO3 v9 LTS
- TYPO3 v10 LTS

## Installation
via Composer (recommended):
```
"require": {
    "orcas/hrs-ibe": "dev-master"
},
"repositories": [
    { "type": "composer", "url": "https://composer.typo3.org/" },
    { "type": "vcs", "url": "https://bitbucket.org/destinationsolutions/ibe-typo3-extension.git" }
 ],
```

## Site configuration
 
1. Create new page on Web > Page
2. Apply new page element to this page (right-click > New)
    - Enter a page title 
    - Go to tab "Plugin" and choose type "IBE View"
    - Save your changes 
3. Enable new page (right-click > enable)
4. Create new template record for this page on Web > Template
    - Enter in template "setup" configuration the following typo3 code:
        ```
        page = PAGE
        page.10 < styles.content.get
        ```
    - Go to "Includes" tab and choose the following available static items
       - Fluid Content Elements
       - Fluid Content Elements CSS
       - XML Sitemap
       - HRS-IBE 
    - Save your changes
5. Create new site configuration for this page on Site Management > Sites 
   - Make following settings:
      - Tab “General”
        - Site identifier: main
        - Entry Point: / 
      - Tab “Languages”
        - Title: German
        - Entry Point: /de
        - Language Key: German [de]
        - Locale: de_DE
        - Two Letter ISO Code: German [de]
   - Save your changes
6. Clear all caches in typo3 backend
7. Follow the steps to configure the IBE. (see Configuration section)
8. IBE should be displayed in frontend now.
9. In your IDE the file ~/app/config/sites/main/config.yaml should be created. Add the following route enhancers to this file:
```yaml
routeEnhancers:
    dsIbe:
        type: Extbase
        extension: HrsIbe
        plugin: Orcas.HrsIbe
        routes:
            - routePath: '/ggv/{p_location}/{p_star}/{p_object}/{p_id}'
              _controller: 'View::list'
              _arguments:
                  p_location: location
                  p_star: star
                  p_object: object
                  p_id: ibe-id
            - routePath: '/ggv/{p_location}/{p_object}/{p_id}'
              _controller: 'View::list'
              _arguments:
                  p_location: location
                  p_object: object
                  p_id: ibe-id
            - routePath: '/{p_locale}/ggv/{p_location}/{p_star}/{p_object}/{p_id}'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
                  p_location: location
                  p_star: star
                  p_object: object
                  p_id: ibe-id
            - routePath: '/{p_locale}/ggv/{p_location}/{p_object}/{p_id}'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
                  p_location: location
                  p_object: object
                  p_id: ibe-id
            - routePath: '/buchen/{p_locale}/book'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
            - routePath: '/buchen/{p_locale}/merkliste'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
            - routePath: '/action'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
            - routePath: '/details/{p_locale}/{p_object}'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
                  p_object: object
            - routePath: '/item/{p_locale}/{p_object}'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
                  p_object: object
            - routePath: '/details/{p_locale}/{p_location}/{p_object}/{p_id}'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
                  p_location: location
                  p_object: object
                  p_id: ibe-id
            - routePath: '/item/{p_locale}/{p_location}/{p_object}/{p_id}'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
                  p_location: location
                  p_object: object
                  p_id: ibe-id
            - routePath: '/details/{p_locale}/{p_location}/{star}/{p_object}/{p_id}'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
                  p_location: location
                  p_star: star
                  p_object: object
                  p_id: ibe-id
            - routePath: '/item/{p_locale}/{p_location}/{star}/{p_object}/{p_id}'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
                  p_location: location
                  p_star: star
                  p_object: object
                  p_id: ibe-id
            - routePath: '/details/{p_locale}/ggv/{p_location}/{p_object}/{p_id}'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
                  p_location: location
                  p_object: object
                  p_id: ibe-id
            - routePath: '/item/{p_locale}/ggv/{p_location}/{p_object}/{p_id}'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
                  p_location: location
                  p_object: object
                  p_id: ibe-id
            - routePath: '/details/{p_locale}/ggv/{p_location}/{star}/{p_object}/{p_id}'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
                  p_location: location
                  p_star: star
                  p_object: object
                  p_id: ibe-id
            - routePath: '/item/{p_locale}/ggv/{p_location}/{star}/{p_object}/{p_id}'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
                  p_location: location
                  p_star: star
                  p_object: object
                  p_id: ibe-id
            - routePath: '/{p_locale}/book'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
            - routePath: '/{p_prefix}/{p_locale}/merkliste'
              _controller: 'View::list'
              _arguments:
                  p_prefix: prefix
                  p_local: locale
            - routePath: '/{p_locale}/merkliste'
              _controller: 'View::list'
              _arguments:
                  p_local: locale
            - routePath: '/{p_locale}/request'
              _controller: 'View::requestFunnel'
              _arguments:
                p_locale: locale
            - routePath: '/{p_prefix}/{p_locale}/request'
              _controller: 'View::requestFunnel'
              _arguments:
                p_prefix: prefix
                p_locale: locale
            - routePath: '/request'
              _controller: 'View::requestFunnel'
``` 

## Configuration
**Authentication configuration**

1. Go to typo3 backend module: Admin Tools > DS IBE
2. Only on first usage: enter the requested api-key and interface-id in the corresponding fields
![Screenshot of the default backend module](Documentation/Images/backend_module.png "Output of the module after api-key and interface-id validation")
3. Click on "Speichern" button

**IBE configuration**

You can enable the "Filter überschreiben" checkbox on "IBE" tab to customize ibe settings. After activating the "Filter überschreiben" checkbox,
all tabs to customize ibe settings are selectable.
 

# CHANGELOG
**1.0.0 - 25.03.2021**

- Remove TYPO3 8 support
- Provide TYPO3 10 support
- Change extension state from alpha to beta

**1.0.1 - 12.04.2021**

- Provide /request route
