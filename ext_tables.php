<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function () {

        if (TYPO3_MODE === 'BE') {

            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'Orcas.HrsIbe',
                'tools', // Make module a submodule of 'tools'
                'ibeconfig', // Submodule key
                '', // Position
                [
                    'IBE' => 'settings, saveSettingsForm',

                ],
                [
                    'access' => 'user,group',
                    'iconIdentifier' => 'hrs_ibe-plugin-ibeview',
                    'labels' => 'LLL:EXT:hrs_ibe/Resources/Private/Language/locallang_ibeconfig.xlf',
                    'navigationComponentId' => 'TYPO3/CMS/Backend/PageTree/PageTreeElement'
                ]
            );

        }
    }
);
