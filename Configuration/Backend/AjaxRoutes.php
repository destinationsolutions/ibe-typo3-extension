<?php
use TYPO3\CMS\Backend\Controller;

return [

    // Expand or toggle in legacy file tree
    'ibe_hrs_region_call' => [
        'path' => '/api/v1/msp/region',
        'target' => Orcas\HrsIbe\Controller\IBEController::class . '::loadMspRegion'
    ]
];