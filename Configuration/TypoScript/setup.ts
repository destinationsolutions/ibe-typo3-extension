plugin.tx_hrsibe_ibeview {
  view {
    templateRootPaths {
      10 = EXT:hrs_ibe/Resources/Private/Templates/
      20 = {$plugin.tx_hrsibe_ibeview.view.templateRootPath}
    }

    partialRootPaths {
      10 = EXT:hrs_ibe/Resources/Private/Partials/
      20 = {$plugin.tx_hrsibe_ibeview.view.partialRootPath}
    }

    layoutRootPaths {
      10 = EXT:hrs_ibe/Resources/Private/Layouts/
      20 = {$plugin.tx_hrsibe_ibeview.view.layoutRootPath}
    }
  }

  persistence {
    storagePid = {$module.tx_hrsibe_ibeconfig.persistence.storagePid}
  }

  features {
    #skipDefaultArguments = 1
    # if set to 1, the enable fields are ignored in BE context
    ignoreAllEnableFieldsInBe = 0
    # Should be on by default, but can be disabled if all action in the plugin are uncached
    requireCHashArgumentForActionArguments = 0
  }

  mvc {
    #callDefaultActionIfActionCantBeResolved = 1
  }
}

# these classes are only used in auto-generated templates
plugin.tx_hrsibe._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hrs-ibe table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hrs-ibe table th {
        font-weight:bold;
    }

    .tx-hrs-ibe table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

# Module configuration
module.tx_hrsibe_tools_hrsibeibeconfig {
  persistence {
    storagePid = {$module.tx_hrsibe_ibeconfig.persistence.storagePid}
  }

  view {
    templateRootPaths {
      10 = EXT:hrs_ibe/Resources/Private/Backend/Templates/
      20 = {$module.tx_hrsibe_ibeconfig.view.templateRootPath}
    }

    partialRootPaths {
      10 = EXT:hrs_ibe/Resources/Private/Backend/Partials/
      20 = {$module.tx_hrsibe_ibeconfig.view.partialRootPath}
    }

    layoutRootPaths {
      10 = EXT:hrs_ibe/Resources/Private/Backend/Layouts/
      20 = {$module.tx_hrsibe_ibeconfig.view.layoutRootPath}
    }
  }
}
