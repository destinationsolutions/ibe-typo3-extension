
plugin.tx_hrsibe_ibeview {
    view {
        # cat=plugin.tx_hrsibe_ibeview/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hrs_ibe/Resources/Private/Templates/
        # cat=plugin.tx_hrsibe_ibeview/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hrs_ibe/Resources/Private/Partials/
        # cat=plugin.tx_hrsibe_ibeview/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hrs_ibe/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hrsibe_ibeview//a; type=string; label=Default storage PID
        storagePid =
    }
}

module.tx_hrsibe_ibeconfig {
    view {
        # cat=module.tx_hrsibe_ibeconfig/file; type=string; label=Path to template root (BE)
        templateRootPath = EXT:hrs_ibe/Resources/Private/Backend/Templates/
        # cat=module.tx_hrsibe_ibeconfig/file; type=string; label=Path to template partials (BE)
        partialRootPath = EXT:hrs_ibe/Resources/Private/Backend/Partials/
        # cat=module.tx_hrsibe_ibeconfig/file; type=string; label=Path to template layouts (BE)
        layoutRootPath = EXT:hrs_ibe/Resources/Private/Backend/Layouts/
    }
    persistence {
        # cat=module.tx_hrsibe_ibeconfig//a; type=string; label=Default storage PID
        storagePid =
    }
}
