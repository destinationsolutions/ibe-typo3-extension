<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:hrs_ibe/Resources/Private/Language/locallang_db.xlf:tx_hrsibe_domain_model_ibesettings',
        'label' => 'setting_key',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'enablecolumns' => [
        ],
        'searchFields' => 'setting_key,value',
        'iconfile' => 'EXT:hrs_ibe/Resources/Public/Icons/tx_hrsibe_domain_model_ibesettings.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'setting_key, value',
    ],
    'types' => [
        '1' => ['showitem' => 'setting_key, value'],
    ],
    'columns' => [
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'setting_key' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hrs_ibe/Resources/Private/Language/locallang_db.xlf:tx_hrsibe_domain_model_ibesettings.setting_key',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'value' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hrs_ibe/Resources/Private/Language/locallang_db.xlf:tx_hrsibe_domain_model_ibesettings.value',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
    ],
];
