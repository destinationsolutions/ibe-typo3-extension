<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Orcas.HrsIbe',
    'ibeview',
    'IBE-View'
);

$pluginSignature = 'hrsibe_ibeview';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:hrs_ibe/Configuration/FlexForms/PluginSettings.xml'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hrs_ibe', 'Configuration/TypoScript', 'HRS-IBE');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hrsibe_domain_model_ibesettings', 'EXT:hrs_ibe/Resources/Private/Language/locallang_csh_tx_hrsibe_domain_model_ibesettings.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hrsibe_domain_model_ibesettings');