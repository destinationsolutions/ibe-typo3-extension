var current = 0;
(function($) {
    var list = document.getElementsByClassName('data-input-field');

    for(var ii = 0; ii < list.length; ii++) {
        if(list[ii].hasAttribute('data-name') && list[ii].getAttribute('data-name').length > 0) {
            list[ii].setAttribute('name', list[ii].getAttribute('data-name'));
        }
    }

    document.getElementsByClassName('tab-bar')[0].onclick = function(e) {
        if(e.target.tagName == 'LABEL' && !e.target.classList.contains('custom-disabled') && e.target.id != "ibe-help-tab-bar") {
            var index =  e.target.getAttribute('data-index');
            this.children[current].classList.remove('current-tab');
            document.getElementsByClassName('tab-content')[current].classList.remove('current-content');

            document.getElementById('ibe-help-tab-content').classList.remove('current-content');
            document.getElementById('ibe-help-tab-bar').classList.remove('current-tab');

            current = index;

            this.children[current].classList.add('current-tab');
            document.getElementsByClassName('tab-content')[current].classList.add('current-content');
        }
    };
    current = 0;

    document.getElementById('wrapper-wrapper_group-custom-enabled-yes').onclick = function() {
        if(this.checked) {
            var bars = document.getElementsByClassName('tab-bar')[0].children;
            for (var i = 0; i < bars.length; i++) {
                bars[i].classList.remove('custom-disabled');
            }
        }
    };

    document.getElementById('wrapper-wrapper_group-custom-enabled-no').onclick = function() {
        if(this.checked) {
            var bars = document.getElementsByClassName('tab-bar')[0].children;
            for (var i = 0; i < bars.length; i++) {
                if (i > 0 && bars[i].id != "ibe-help-tab-bar") {
                    bars[i].classList.add('custom-disabled');
                }
            }
        }
    };

    document.getElementById('save-settings').onclick = function() {
        this.style.display = 'none';
        document.getElementById('ibe-save-loader').style.display = 'block';
    };

    document.getElementById('ibe-help-tab-bar').onclick = function() {
        var bars = document.getElementsByClassName('tab-bar')[0].children;
        for (var i = 0; i < bars.length; i++) {
            bars[i].classList.remove('current-tab');
        }

        var contents = document.getElementsByClassName('tab-content');
        for (var j = 0; j <contents.length; j++) {
            contents[j].classList.remove('current-content');
        }

        this.classList.add('current-tab');
        document.getElementById('ibe-help-tab-content').classList.add('current-content');
    };

    var dataCityContainer = '';
    var list = document.getElementsByClassName('input-selectize');

    //for(var ii = 0; ii < list.length; ii++) {
    var input = document.getElementById('booking_form-region');
    var textarea = input.previousElementSibling;
    input._dataInput = textarea.previousElementSibling;
    dataCityContainer = JSON.parse(textarea.textContent);
    var dataSource = dataCityContainer;

    $('#' + input.getAttribute('id')).selectize({
        valueField: 'value',
        labelField: 'name',
        searchField: ['name', 'value', 'zip', 'search'],
        options: Object.values(dataSource),
        onChange: function(value) {
            var input = this.$input[0]._dataInput;
            var data = this.items;
            var result = [];

            for(var ii = 0; ii < data.length; ii++) {
                var id = data[ii];
                var item = dataSource[id];
                result.push({name:dataSource[id]['name'], value:id});
            }

            input.value = JSON.stringify(result);
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: TYPO3.settings.ajaxUrls['ibe_hrs_region_call'],
                type: 'GET',
                dataType: 'json',
                data: {
                    search: query,
                    server: $('#input-server').val()
                },
                error: function() {
                    callback();
                },
                success: function(res) {
                    var data = [];

                    for(var ii = 0; ii < res.length; ii++) {
                        var item = res[ii];
                        data.push({'name': item.label, 'search': item.value, 'zip': 'unknown', 'value': item.value});
                    }

                    callback(data);
                }
            });
        },
        render: {
            item: function(item, escape) {
                return '<div class="selection">' +
                    (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +
                    (item.zip ? '<span class="data">Zip: ' + escape(item.zip) + '</span>' : '') +
                    '</div>';
            },
            option: function(item, escape) {
                var label = item.name || item.zip;
                var caption = item.name ? 'zip: ' + item.zip : null;
                return '<div class="option">' +
                    '<span class="label">' + escape(label) + '</span>' +
                    (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
                    '</div>';
            }
        }
    });

    var input = document.getElementById('filter-city');
    var textarea = input.previousElementSibling;
    input._dataInput = textarea.previousElementSibling;
    var dataSource = dataCityContainer;

    $('#' + input.getAttribute('id')).selectize({
        valueField: 'name',
        labelField: 'name',
        searchField: ['name', 'value', 'zip', 'search'],
        options: Object.values(dataSource),
        onChange: function(value) {
            var input = this.$input[0]._dataInput;
            var data = this.items;
            var result = [];

            for(var ii = 0; ii < data.length; ii++) {
                var id = data[ii];
                result.push(id);
            }

            input.value = JSON.stringify(result);
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: TYPO3.settings.ajaxUrls['ibe_hrs_region_call'],
                type: 'GET',
                dataType: 'json',
                data: {
                    search: query,
                    server: $('#input-server').val()
                },
                error: function() {
                    callback();
                },
                success: function(res) {
                    var data = [];

                    for(var ii = 0; ii < res.length; ii++) {
                        var item = res[ii];
                        data.push({'name': item.label, 'search': item.value, 'zip': 'unknown', 'value': item.value});
                    }

                    callback(data);
                }
            });
        },
        render: {
            item: function(item, escape) {
                return '<div class="selection">' +
                    (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +
                    (item.zip ? '<span class="data">Zip: ' + escape(item.zip) + '</span>' : '') +
                    '</div>';
            },
            option: function(item, escape) {
                var label = item.name || item.zip;
                var caption = item.name ? 'zip: ' + item.zip : null;
                return '<div class="option">' +
                    '<span class="label">' + escape(label) + '</span>' +
                    (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
                    '</div>';
            }
        }
    });
    //}
})(window.jQuery2_x);
