<?php
/**
 * Created by PhpStorm.
 * User: michael.kirchner
 * Date: 28.08.19
 * Time: 14:47
 */

$data = array(
    'wrapper' => array('custom' => array('enabled' => true)),
    'general' => array(
        'region' => array(
            'enabled' => true,
            'values' => "we44aae8f9ad570be6ee"
        ),
        'firm' => array(
            'enabled' => true,
            'values' => ""
        ),
        'aphrodite_interface_id' => array(
            'enabled' => true,
            'values' => ""
        ),
        'trustYouRatingEnabled' => array(
            'enabled' => true,
        ),
        'hostEnabled' => array(
            'enabled' => true,
        ),
        'aphroditeStarsEnabled' => array(
            'enabled' => true,
        ),
        'im_web_interfaces' => array(
            'enabled' => true,
            'values' => array("interface1", "interface2", "wwwwqweqweqw") //Textarea einträge zeilenweise
        ),
        'maxEntries' => array(
            'enabled' => true,
            'values' => 50
        ),
        'successMailEnabled' => array(
            'enabled' => true,
        ),
        'disabledIds' => array(
            'enabled' => true,
            'values' => array('objectid1', 'objectid2', 'objectid3') //Textarea einträge zeilenweise
        ),
        'uniqueTextEnabled' => array(
            'enabled' => true,
        ),
        'show_on_request' => array(
            'enabled' => true,
            'values' => 2
        ),
        'discountLimit' => array(
            'enabled' => true,
            'values' => 22
        ),
        'globalErvEnabled' => array(
            'enabled' => true,
        ),
        'mailCopy' => array(
            'enabled' => true,
            'values' => array("example@exampl.de", "info@info.de")//Textarea einträge zeilenweise
        ),
        'popoverCalendar' => array(
            'enabled' => true,
        ),
        'privacyUrl' => array(
            'enabled' => true,
            'values' => "http://example.de"
        ),
    ),
    'libs' => array(
        'google' => array(
            'enabled' => true,
            'api_key' => 'ejwriojwrewrhwriwhriwuhruwiwiwhiwui'
        ),
        'map' => array(
            'enabled' => true,
            'center' =>  array('lat' => 11.4552, 'lng' => 45.125),
            'bounding_box' => array('ne' => '55.078351,8.569488', 'sw' => '54.723672,8.166091'),
            'zoom' => 5
        ),
    ),
    'booking_form' => array(
        'region' => array(
            'enabled' => true,
            'values' => array(
                array('name' => 'Sylt', 'value' => 'we59ce85723fd406abbb'),
                array('name' => 'Westerland', 'value' => 'we34f44dbe2e47f63a8a')
            )
        )
    ),
    'search' => array(
        'threshold' => array(
            'enabled' => true,
            'values' => 5
        ),
        'multiRoomSearch' => array(
            'enabled' => true,
        ),
        'alternatives' => array(
            'enabled' => true,
            'duration' => array(
                'enabled' => true,
                'maxAlternativesShown' => 10,
                'durationFutureOffset' => 5,
                'durationPastOffset' => 5
            ),
            'noFilter' => array(
                'enabled' => true,
                'maxAlternativesShown' => 10,
            ),
        )
    ),
    'sort' => array(
        'unsortedEnabled' => array(
            'enabled' => false,
        ),
        'relevanceEnabled' => array(
            'enabled' => false,
        ),
        'endpoints' => array(
            'enabled' => false,
            'values' => array(1,2,3,4,5, "204") //Textarea einträge zeilenweise
        ),
        'default' => array(
            'enabled' => false,
            'values' => 'minStar'
        ),
        'order' => array(
            'enabled' => false,
            'values' => 'asc'
        ),
        'orderByMinPriceIfStarOrdering' => array(
            'enabled' => false,
        )
    ),
    'view' => array(
        'contactBoxEnabled' => array(
            'enabled' => false,
        ),
        'cartEnabled' => array(
            'enabled' => false,
        ),
        'cartAutoCheckoutButtonEnabled' => array(
            'enabled' => false,
        ),
        'shortageEnabled' => array(
            'enabled' => false,
        ),
        'bookingInformation' => array(
            'enabled' => false,
            'values' => "Text"
        ),
        'dtvEnabled' => array(
            'enabled' => false
        ),
        'ggvOnRequestButtonEnabled' => array(
            'enabled' => false
        ),
        'ggvBookingBoxEnabled' => array(
            'enabled' => false
        ),
        'winterImage' => array(
            'enabled' => false,
            'startMonth' => 9,
            'endMonth' => 2
        ),
        'climaCalculatorEnabled' => array(
            'enabled' => false,
        ),
        'successsCustomTextEnabled' => array(
            'enabled' => false,
        ),
        'gridLayoutEnabled' => array(
            'enabled' => false,
        ),
        'bookingKit' => array(
            'enabled' => false,
            'DOMname_formblatt' => "String",
            'geolocation' => array(
                'ne' => 'String',
                'sw' => 'String'
            )
        )
    ),
    //Kommen noch mehr filter dazu
    'filter' => array(
        'price' => array(
            'enabled' => false,
            'min' => 300,
            'max' => 500,
            'open' => false
        ),
        'city' => array(
            'enabld' => true,
            'values' => array('spaßhausen'),
            'open' => false
        )
    )
);