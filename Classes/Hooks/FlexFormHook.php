<?php
namespace Orcas\HrsIbe\Hooks;

use Orcas\HrsIbe\Domain\Model\IbeSettings;
use Orcas\HrsIbe\Domain\Repository\IbeSettingsRepository;
use Orcas\HrsIbe\Http\IBERequest;
use TYPO3\CMS\Extbase\Annotation\Inject;


class FlexFormHook
{
    /**
     * ibeSettingsRepository
     *
     * @Inject
     * @var \Orcas\HrsIbe\Domain\Repository\IbeSettingsRepository
     */
    protected $ibeSettingsRepository = null;
    protected $dataStructure = array();
    protected $translations = array();
    protected $translationKeyMap = array(
        'name' => 'description',
        'object_category' => 'oCategory',
        'object_facility' => 'oFacility',
        'room_facility' => 'rFacility',
        'food_drink' => 'oFacility2',
        'sustainability' => 'sustainability',
        'bed' => 'beds',
        'star' => 'minStar',
        'rating' => 'ratingCount',
    );

    protected $ignoreField = array('name');

    protected $suffix = array(
        ''
    );

    public function checkFlexFormValue_beforeMerge($datahandler, &$currentVal, &$newVal) {
        if(is_array($currentVal['data']['sDEF']['lDEF'])){
            foreach ($currentVal['data']['sDEF']['lDEF'] as $filterName => $filter) {
                if(is_array($filter['vDEF'])) {
                    foreach ($filter['vDEF'] as $key => $val) {
                        if(!is_array($newVal['data']['sDEF']['lDEF'][$filterName]['vDEF']) || !$newVal['data']['sDEF']['lDEF'][$filterName]['vDEF'][$key]) {
                            unset($currentVal['data']['sDEF']['lDEF'][$filterName]['vDEF'][$key]);
                        }
                    }
                }
            }
        }
    }


    // For 8x
    /**
     * @param array $dataStructure
     * @param array $identifier
     * @return array
     */
    public function parseDataStructureByIdentifierPostProcess(array $dataStructure, array $identifier)
    {
        $this->dataStructure = $dataStructure;

        if ($identifier['tableName'] === 'tt_content' && $identifier['dataStructureKey'] === 'hrsibe_ibeview,list') {
            $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
            $this->ibeSettingsRepository = $objectManager->get('Orcas\HrsIbe\Domain\Repository\IbeSettingsRepository');

            //load ibe data
            $ibe = new IBERequest($this->getInterface()); // initilisieren mit interface
            $ibe->setApiKey($this->getApiKey()); // API Key für die API der iBe

            if(strlen($this->getIbeServer()) > 0) {
                $ibe->setApiRoute($this->getIbeServer());
                $ibe->setDevServerAuthentication();
            }

            $settings = $ibe->loadConfiguration();

            $file = \TYPO3\CMS\Core\Core\Environment::getPublicPath() . '/' . 'typo3conf/ext/hrs_ibe/Configuration/FlexForms/PluginSettings.xml';
            $content = file_get_contents($file);

            if ($content) {
                $this->translations = $settings['system.translation'];
                $this->dataStructure['sheets']['extraEntry'] = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array($content);

                if(is_array($settings)) {
                    foreach($settings['system.filter'] as $fieldName => $field) {
                        $values = $field['values'];
                        $key = isset($this->translationKeyMap[$fieldName]) ? $this->translationKeyMap[$fieldName]: $fieldName;
                        if(isset($settings['filter.' . $key . '.enabled']) && $settings['filter.' . $key . '.enabled'] == 1 && isset($settings['filter.' . $key . '.values'])) {
                            $values = $settings['filter.' . $key . '.values'];
                        }

                        if(in_array($fieldName, $this->ignoreField)) {
                            continue;
                        }
                        $this->addField($fieldName, is_array($values) && count($values) > 0 ? 'check_select' : 'input', $values, isset($field['translationKey']) ? $field['translationKey'] : null);
                    }
                }
            }
        }

        return $this->dataStructure;
    }


   private function addField($name, $type, $value = array(), $translationKey = null) {
            $translationKey = $translationKey ? $translationKey : $name;
            $translationKey = isset($this->translationKeyMap[$translationKey]) ? $this->translationKeyMap[$translationKey] : $translationKey;

            $field = array(
                'label' => 'LLL:EXT:hrs_ibe/Resources/Private/Language/locallang.xlf:' . $translationKey,
                'config'=> array(
                    'type' => $type == 'check_select' ? 'selectCheckBox' : $type,
                )
            );

            if($type == 'check_select' && is_array($value) && count($value) > 0) {
                $field['config']['minitems'] = 0;
                $field['config']['maxitems'] = 1000;
                $field['config']['renderMode'] = 'checkbox';
                $field['config']['allowNonIdValues'] = true;
                $field['config']['items'] = array();

                foreach($value as $checkboxName) {
                    $key = $checkboxName;

                    if(isset($this->translations[$name.'_'.$checkboxName])) {
                        $checkboxName = $this->translations[$name.'_'.$checkboxName];
                    }
                    $field['config']['items'][] = array($checkboxName, $key);
                }
            }

            if(
                isset($this->dataStructure['sheets']['extraEntry']['sheets']['sDEF']['ROOT']['el'])
                && isset($this->dataStructure['sheets']['sDEF']['ROOT']['el'])
            ) {
                $this->dataStructure['sheets']['extraEntry']['sheets']['sDEF']['ROOT']['el']["settings.page.filter.$translationKey"]['TCEforms'] =
                $this->dataStructure['sheets']['sDEF']['ROOT']['el']["settings.page.filter.$translationKey"]['TCEforms'] = $field;
            }
        }

    private function getSettingsField($key){
            /** @var IbeSettings $settings */
            $settings = $this->ibeSettingsRepository->findByKey($key);

            if($settings->count() == 0){
            return null;
        }

        return $settings->toArray()[0];
    }

    private function getInterface() {
        $field = $this->getSettingsField('hrs_ibe_interface_id');
        if($field) {
            return $field->getValue();
        }

        return '';
    }

    private function getApiKey() {
        $field = $this->getSettingsField('hrs_ibe_api_key');
        if($field) {
            return $field->getValue();
        }

        return '';
    }

    private function getIbeServer() {
        $field = $this->getSettingsField('hrs_ibe_server');
        if($field) {
            return $field->getValue();
        }

        return 'https://ibe.ds-destinationsolutions.com';
    }
}
