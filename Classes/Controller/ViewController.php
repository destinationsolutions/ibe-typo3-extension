<?php

namespace Orcas\HrsIbe\Controller;

use Orcas\HrsIbe\Domain\Model\IbeSettings;
use Orcas\HrsIbe\Http\IBERequest;

class ViewController extends AbstractController
{

    public function listAction() {
        if(isset($_GET['tx_hrsibe_orcas.hrsibe'])) {
            $_GET = array_merge($_GET, $_GET['tx_hrsibe_orcas.hrsibe']);
            unset($_GET['tx_hrsibe_orcas.hrsibe']);
        }

        $ibe = new IBERequest($this->getInterface()); // initilisieren mit interface
        $ibe->setApiKey($this->getApiKey()); // API Key für die API der iBe
        $ibe->setFieldPrefix('ibe'); //alle parameter der eibe erhalten einen Prefix um diese von den seitenparametern zu trennen
        if($this->getSeo() == 1) {
            $uri = $this->controllerContext
                ->getUriBuilder()
                ->reset()
                ->setTargetPageUid($GLOBALS['TSFE']->id)
                ->buildFrontendUri();
            $ibe->setUrlPrefix($uri);
            $ibe->setJsUrlPrefix(substr($uri, 1));
        } //ein prefix die an den anfang des seitenpfades gesetzt wird
        $ibe->setPluginPath('/ext/hrs_ibe/Resources/Public/css'); //Absuluter webpath zu den ibe bundle verzeichnis
        $ibe->setSinglePage($this->getSeo() == 0); //ersetzt alle pfade der ibe mit den aktuellen seitenpfad und nutzt die getparameter zur ermittlung derrichtigen seite
        $ibe->setApiRoute($this->getIbeServer());
        $ibe->setDevServerAuthentication();

        //prefill filter
        //settings comes from 'ActionController' from T3 itself
        $filterList = $this->settings['page']['filter'];

        if(is_array($filterList)) {
            foreach ($filterList as $key => $value) {
                if ((is_array($value) && count($value) > 0) || strlen($value) > 0) {
                    if (!is_array($value) && strpos($value, ',') !== false) {
                        $ibe->addParam($key, explode(',', $value));
                    } else {
                        $ibe->addParam($key, $value);
                    }
                }
            }
        }

        $template = $this->getCustomTemplates();
        $hasCustomTemplate = $ibe->getCustomPageKey();
        $renderPage = true;

        if($hasCustomTemplate && isset($template[$hasCustomTemplate])) {
            $template = $template[$hasCustomTemplate];
            if(strlen(trim($template)) > 0) {
                //EXT:hrs_ibe/Resources/Private/Templates/
                $templatePath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($template);
                $this->view->setTemplatePathAndFilename($templatePath);
                $renderPage = false;
            }
        }

        $ibepage = $renderPage ? $ibe->getPage() : $ibe->getPageAsJson(); //lädt automatisch an hand er übergebenen sdaten die korrekte seite

        if($renderPage) {
            preg_match_all('/\<link.*?\>/', $ibepage, $matches);
            preg_match('/\<title.*?\<\/title\>/', $ibepage, $title);

            if($title) {
                $ibepage = str_replace($title[0], '', $ibepage);
                $GLOBALS['TSFE']->page['title'] = str_replace(array('<title>', '</title>'), '', $title[0]);
            }

            if ($matches) {
                foreach ($matches[0] as $key => $match) {
                    $GLOBALS['TSFE']->additionalHeaderData['ibe_' . $key] = $match;
                }
                $ibepage = str_replace($matches[0], '', $ibepage);
            }
        }

        $this->signalSlotDispatcher->dispatch(__CLASS__, 'afterPageFetched', ['ibepage' => &$ibepage]);

        $this->view->assign('ibepage', $ibepage);
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }
}
