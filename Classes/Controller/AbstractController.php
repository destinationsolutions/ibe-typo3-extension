<?php
namespace Orcas\HrsIbe\Controller;

use Orcas\HrsIbe\Domain\Model\IbeSettings;
use Orcas\HrsIbe\Domain\Repository\IbeSettingsRepository;
use Orcas\HrsIbe\Http\IBERequest;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Annotation\Inject;

/**
 * Class AbstractController
 *
 * @package Orcas\HrsIbe\Controller
 * @author  Mandy Vorköper <mandy.vorkoeper@ds-destinationsolutions.com>
 */
abstract class AbstractController extends ActionController
{
    /**
     * ibeSettingsRepository
     *
     * @Inject
     * @var \Orcas\HrsIbe\Domain\Repository\IbeSettingsRepository
     */
    protected $ibeSettingsRepository;

    /**
     * Retrieve jrs_ibe_server field from t3 database table
     * tx_hrsibe_domain_model_ibesettings.
     *
     * @return string
     */
    protected function getIbeServer()
    {
        $field = $this->getSettingsField('hrs_ibe_server');

        if($field && strlen(trim($field->getValue())) > 0) {
            return $field->getValue();
        }

        return IBERequest::IBE_SERVER_PROD;
    }

    /**
     * Retrieve hrs_ibe_interface_id field from t3 database table
     * tx_hrsibe_domain_model_ibesettings.
     *
     * @return string
     */
    protected function getInterface() {
        $field = $this->getSettingsField('hrs_ibe_interface_id');
        if($field) {
            return $field->getValue();
        }

        return '';
    }

    /**
     * Retrieve hrs_ibe_seo field from t3 database table
     * tx_hrsibe_domain_model_ibesettings.
     *
     * @return bool|string
     */
    protected function getSeo() {
        $field = $this->getSettingsField('hrs_ibe_seo');
        if($field) {
            return $field->getValue();
        }

        return false;
    }

    /**
     * Retrieve hrs_ibe_api_key field from t3 database table
     * tx_hrsibe_domain_model_ibesettings.
     *
     * @return string
     */
    protected function getApiKey() {
        $field = $this->getSettingsField('hrs_ibe_api_key');
        if($field) {
            return $field->getValue();
        }

        return '';
    }

    /**
     * Retrieve template field from t3 database table
     * tx_hrsibe_domain_model_ibesettings.
     *
     * @return array|mixed
     */
    protected function getCustomTemplates() {
        $field = $this->getSettingsField('template');
        if($field) {
            return json_decode($field->getValue(), true);
        }

        return array();
    }

    protected function saveSettingsField($key, $value){
        $setting = $this->getSettingsField($key);

        if($setting == null){
            $setting = new IbeSettings();
            $setting->setSettingKey($key);
            $setting->setValue($value);
            $this->ibeSettingsRepository->add($setting);
        }
        else {
            $setting->setValue($value);
            $this->ibeSettingsRepository->update($setting);
        }


        $persistenceManager = $this->objectManager->get("TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager");
        $persistenceManager->persistAll();
    }

    /**
     * @param $key
     * @return IbeSettings
     */
    private function getSettingsField($key){
        $settings = $this->ibeSettingsRepository->findByKey($key);

        if($settings->count() == 0){
            return null;
        }

        return $settings->toArray()[0];
    }
}
