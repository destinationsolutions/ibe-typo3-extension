<?php
/**
 * Created by PhpStorm.
 * User: michael.kirchner
 * Date: 28.08.19
 * Time: 12:35
 */

namespace Orcas\HrsIbe\Controller;

use Orcas\HrsIbe\Http\IBERequest;

class IBEController extends AbstractController
{

    /**
     * Stores and retrieves ibe settings.
     */
    public function settingsAction(){
        $valid = $this->saveSettings();
        $settings = $this->getFieldTemplate();

        $this->view->assign('settings', $settings);
        $this->view->assign('valid', $valid);
        $this->view->assign('send', isset($_POST) && count($_POST) > 0);
        $this->view->assign('server', $this->getIbeServer());
        $this->view->assign('api_key', $this->getApiKey());
        $this->view->assign('interface', $this->getInterface());
    }


    public function loadMspRegion() {
        $ibe = new IBERequest('anonymous');
        if(strlen(trim($_GET['server'])) > 0) {
            $ibe->setApiRoute($_GET['server']);
        }
        $ibe->addParam('search', $_GET['search']);
        $ibe->ajax('msp-region');
    }

    /**
     * Stores api settings in t3 database table
     * tx_hrsibe_domain_model_ibesettings.
     */
    private function saveApiSettings() {
        $this->saveSettingsField('hrs_ibe_interface_id', $_POST['interface']);
        $this->saveSettingsField('hrs_ibe_api_key', $_POST['api_key']);
        $this->saveSettingsField('hrs_ibe_server', $_POST['server']);
        $this->saveSettingsField('hrs_ibe_seo', $_POST['general']['seo']['enabled']);
    }

    private function saveTemplateSettings() {
        $this->saveSettingsField('template', json_encode($_POST['template']));
    }

    private function saveSettings() {
        if(isset($_POST['save-settings'])) {
            if(!isset($_POST['authentication-required'])) {
                $this->saveApiSettings();
                $this->saveTemplateSettings();
                $ibe = new IBERequest($this->getInterface()); // initialisieren mit interface
                $ibe->setApiKey($this->getApiKey()); // API Key für die API der iBe

                if(strlen($this->getIbeServer()) > 0) {
                    $ibe->setApiRoute($this->getIbeServer());
                    $ibe->setDevServerAuthentication();
                }

                if(!$ibe->validateApiKey()) {
                    return false;
                }

                unset($_POST['server']);
                unset($_POST['api_key']);
                unset($_POST['save-settings']);
                unset($_POST['interface']);
                //unset($_POST['seo']);
                unset($_POST['authentication-required']);
                unset($_POST['template']);

//                $filteredRegions = array();
//                foreach($_POST['booking_form']['region']['values'] as $item) {
//                    if(isset($item['value'])) {
//                        $filteredRegions[] = $item;
//                    }
//                }
                $_POST['booking_form']['region']['values'] = json_decode($_POST['booking_form']['region']['values'], true);
                $_POST['filter']['city']['values'] = json_decode($_POST['filter']['city']['values'], true);

                $ibe->saveConfiguration($_POST);
                return true;
            } else {
                //TODO check here api key and api key;
                //TODO save here api key and interface in typo3
                $this->saveApiSettings();
                $ibe = new IBERequest($this->getInterface()); // initilisieren mit interface
                $ibe->setApiKey($this->getApiKey()); // API Key für die API der iBe
                if(strlen($this->getIbeServer()) > 0) {
                    $ibe->setApiRoute($this->getIbeServer());
                    $ibe->setDevServerAuthentication();
                }
                return $ibe->validateApiKey();
            }
        }
        return true;
    }

    private function getFieldTemplate() {
        $ibe = new IBERequest($this->getInterface()); // initilisieren mit interface
        $ibe->setApiKey($this->getApiKey()); // API Key für die API der iBe

        if(strlen($this->getIbeServer()) > 0) {
            $ibe->setApiRoute($this->getIbeServer());
            $ibe->setDevServerAuthentication();
        }

        $settings = $ibe->loadConfiguration();
        $template = $this->getCustomTemplates();

        if(is_array($settings)) {
            $imWebInterfaces = array();
            $selectedInterfaces = isset($settings['general.im_web_interfaces.values']) ? $settings['general.im_web_interfaces.values'] : array();
            if(isset($settings['general.im_web_interfaces.values']) && !is_array($settings['general.im_web_interfaces.values'])) {
                $selectedInterfaces = array();
            }
            foreach ($settings['system.general.im_web_interfaces.values'] as $value) {
                $imWebInterfaces[] = array('value' => $value, 'name' => $value, 'enabled' => in_array($value, $selectedInterfaces));
            }

            $cities = array();
            $selectedCities = isset($settings['filter.city.values']) ? $settings['filter.city.values'] : array();
            if(isset($settings['filter.city.values']) && !is_array($settings['filter.city.values'])) {
                $selectedCities = array();
            }
            foreach ($settings['system.filter']['city']['values'] as $city) {
                $cities[] = array('value' => $city, 'name' => $city, 'enabled' => in_array($city, $selectedCities));
            }

            $endPointCollection = array();
            $endPointsSelected = array();
            if(isset($settings['sort.endpoints.values']) && is_array($settings['sort.endpoints.values'])) {
                $endPointsSelected = array_column($settings['sort.endpoints.values'], 'value');
                if(!is_array($endPointsSelected)) {
                    $endPointsSelected = array();
                }
            }
            foreach ($settings['system.sort.endpoints'] as $end) {
                $endPointCollection[] = array('value' => $end, 'name' => $end, 'enabled' => in_array($end, $endPointsSelected));
            }

            $bookingFormRegionCollection = array();
            $bookingFormRegionEnabledCollection = array();
            $regionSelected = array();
            if(isset($settings['booking_form.region.values']) && is_array($settings['booking_form.region.values'])) {
                $regionSelected = array_column($settings['booking_form.region.values'], 'value');
                if(!is_array($regionSelected)) {
                    $regionSelected = array();
                }
            }

            foreach ($settings['system.booking_form.region'] as $region) {
                $bookingFormRegionCollection[$region['value']] = array('value' => $region['value'], 'name' => $region['name'], 'zip' => $region['zip'], 'search' => join(',', $region['search']));
                if(in_array($region['value'], $regionSelected)) {
                    $bookingFormRegionEnabledCollection[] = $region['value'];
                }
            }

            $sort = array(
                array('value' => 'unsorted', 'enabled' => isset($settings['sort.default.values']) && 'unsorted' == $settings['sort.default.values']),
                array('value' => 'description', 'enabled' => isset($settings['sort.default.values']) && 'description' == $settings['sort.default.values']),
                array('value' => 'minPrice', 'enabled' => isset($settings['sort.default.values']) && 'minPrice' == $settings['sort.default.values']),
                array('value' => 'city', 'enabled' => isset($settings['sort.default.values']) && 'city' == $settings['sort.default.values']),
                array('value' => 'minStar', 'enabled' => isset($settings['sort.default.values']) && 'minStar' == $settings['sort.default.values']),
                array('value' => 'ratingCount', 'enabled' => isset($settings['sort.default.values']) && 'ratingCount' == $settings['sort.default.values']),
                array('value' => 'relevance', 'enabled' => isset($settings['sort.default.values']) && 'relevance' == $settings['sort.default.values'])
            );

            $order = array(
                array('value' => 'asc', 'enabled' => 'asc' == isset($settings['sort.order.values']) && $settings['sort.order.values']),
                array('value' => 'desc', 'enabled' => 'desc' == isset($settings['sort.order.values']) && $settings['sort.order.values']),
            );

            $data = array(
                'wrapper' => array(
                    'wrapper_group' => array(
                        'noEnabler' => true,
                        'custom' => array(
                            'name' => 'wrapper[custom][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['wrapper.custom.enabled']) && $settings['wrapper.custom.enabled']
                        ),
                        'api_key' => array(
                            'name' => 'api_key',
                            'type' => 'text',
                            'value' => $this->getApiKey()
                        ),
                        'interface' => array(
                            'name' => 'interface',
                            'type' => 'text',
                            'value' => $this->getInterface()
                        ),
                        'server' => array(
                            'name' => 'server',
                            'type' => 'text',
                            'value' => $this->getIbeServer()
                        )
                    ),
                    //'custom' => array('enabled' => $settings['wrapper.custom.enabled'])
                ),
                'general' => array(
                    'general.group' => array(
                        'noEnabler' => true,
                        'seo' => array(
                            'name' => 'general[seo][enabled]',
                            'type' => 'checkbox',
                            'value' => $this->getSeo()
                        ),
                        'trustYouRatingEnabled' => array(
                            'name' => 'general[trustYouRatingEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['general.trustYouRatingEnabled.enabled']) && $settings['general.trustYouRatingEnabled.enabled']
                        ),
                        'hostEnabled' => array(
                            'name' => 'general[hostEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['general.hostEnabled.enabled']) && $settings['general.hostEnabled.enabled']
                        ),
                        'aphroditeStarsEnabled' => array(
                            'name' => 'general[aphroditeStarsEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['general.aphroditeStarsEnabled.enabled']) && $settings['general.aphroditeStarsEnabled.enabled']
                        ),
                        'successMailEnabled' => array(
                            'name' => 'general[successMailEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['general.successMailEnabled.enabled']) && $settings['general.successMailEnabled.enabled']
                        ),
                        'uniqueTextEnabled' => array(
                            'name' => 'general[uniqueTextEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['general.uniqueTextEnabled.enabled']) && $settings['general.uniqueTextEnabled.enabled']
                        ),
                        'globalErvEnabled' => array(
                            'name' => 'general[globalErvEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['general.globalErvEnabled.enabled']) && $settings['general.globalErvEnabled.enabled']
                        ),
                        'popoverCalendar' => array(
                            'name' => 'general[popoverCalendar][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['general.popoverCalendar.enabled']) && $settings['general.popoverCalendar.enabled']
                        ),
                    ),
//                    'region' => array(
//                        'enabled' => isset($settings['general.region.enabled']) && $settings['general.region.enabled'],
//                        'values' => array(
//                            'type' => 'text',
//                            'value' => isset($settings['general.region.values']) && $settings['general.region.values'] ? $settings['general.region.values'] : ''
//                        )
//                    ),
//                    'firm' => array(
//                        'enabled' => isset($settings['general.firm.enabled']) && $settings['general.firm.enabled'],
//                        'values' => array(
//                            'type' => 'text',
//                            'value' => isset($settings['general.firm.values']) && $settings['general.firm.values'] ? $settings['general.firm.values'] : '',
//                        )
//                    ),
//                    'aphrodite_interface_id' => array(
//                        'enabled' => isset($settings['general.aphrodite_interface_id.enabled']) && $settings['general.aphrodite_interface_id.enabled'],
//                        'values' => array('type' => 'text', 'value' => isset($settings['general.aphrodite_interface_id.values']) ? $settings['general.aphrodite_interface_id.values'] : '')
//                    ),
//                    'im_web_interfaces' => array(
//                        'enabled' => isset($settings['general.im_web_interfaces.enabled']) && $settings['general.im_web_interfaces.enabled'],
//                        'values' => array(
//                            'type' => 'checkboxes',
//                            'value' => $imWebInterfaces
//                        )
//                    ),
                    'maxEntries' => array(
                        'enabled' => isset($settings['general.maxEntries.enabled']) && $settings['general.maxEntries.enabled'],
                        'values' => array('type' => 'text', 'value' => isset($settings['general.maxEntries.values']) ? $settings['general.maxEntries.values'] : '')
                    ),
                    'disabledIds' => array(
                        'enabled' => isset($settings['general.disabledIds.enabled']) && $settings['general.disabledIds.enabled'],
                        'values' => array(
                            'type' => 'textarea',
                            'value' => isset($settings['general.disabledIds.values']) ? $settings['general.disabledIds.values'] : ''
                        )
                    ),
                    'show_on_request' => array(
                        'enabled' => isset($settings['general.show_on_request.enabled']) && $settings['general.show_on_request.enabled'],
                        'values' => array(
                            'type' => 'select',
                            'selected' => isset($settings['general.show_on_request.values']) ? $settings['general.show_on_request.values'] : '',
                            'value' => array(
                                array('key' => 0, 'label' => 'nur buchbare Betriebe'),
                                array('key' => 1, 'label' => 'nur Anfragebetriebe'),
                                array('key' => 2, 'label' => 'Alle'),
                            )
                        )
                    ),
                    'discountLimit' => array(
                        'enabled' => isset($settings['general.discountLimit.enabled']) && $settings['general.discountLimit.enabled'],
                        'values' => array(
                            'type' => 'text',
                            'value' => isset($settings['general.discountLimit.values']) ? $settings['general.discountLimit.values'] : ''
                        )
                    ),
                    'mailCopy' => array(
                        'enabled' => isset($settings['general.mailCopy.enabled']) && $settings['general.mailCopy.enabled'],
                        'values' => array(
                            'type' => 'textarea',
                            'value' => isset($settings['general.mailCopy.values']) ? $settings['general.mailCopy.values'] : ''
                        )//Textarea einträge zeilenweise
                    ),
                    'privacyUrl' => array(
                        'enabled' => isset($settings['general.privacyUrl.enabled']) && $settings['general.privacyUrl.enabled'],
                        'values' => array(
                            'type' => 'text',
                            'value' => isset($settings['general.privacyUrl.values']) ? $settings['general.privacyUrl.values'] : ''
                        )
                    ),
                ),
                'libs' => array(
                    'google' => array(
                        'enabled' => isset($settings['libs.google.enabled']) && $settings['libs.google.enabled'],
                        'api_key' => array('type' => 'text', 'value' => isset($settings['libs.google.api_key']) ? $settings['libs.google.api_key'] : '')
                    ),
                    'map_type' => array(
                        'enabled' => isset($settings['libs.map_type.enabled']) && $settings['libs.map_type.enabled'],
                        'values' => array(
                            'type' => 'select',
                            'selected' => isset($settings['libs.map_type.values']) ? $settings['libs.map_type.values'] : '',
                            'value' => array(
                                array('key' => 'google', 'label' => 'google'),
                                array('key' => 'leaflet', 'label' => 'leaflet'),
                            )
                        )
                    ),
                    'map' => array(
                        'enabled' => isset($settings['libs.map.enabled']) && $settings['libs.map.enabled'],
                        'center' => array(
                            'type' => 'empty',
                        ),
                        'center.lng' => array(
                            'class' => 'keymap-value',
                            'type' => 'text',
                            'name' => 'libs[map][center][lng]',
                            'value' => isset($settings['libs.map.center']['lng']) ? $settings['libs.map.center']['lng'] : ''
                        ),
                        'center.lat' => array(
                            'class' => 'keymap-value',
                            'type' => 'text',
                            'name' => 'libs[map][center][lat]',
                            'value' => isset($settings['libs.map.center']['lat']) ? $settings['libs.map.center']['lat'] : ''
                        ),
                        'bounding_box' => array(
                            'type' => 'empty',
                        ),
                        'bounding_box.ne' => array(
                            'class' => 'keymap-value',
                            'type' => 'text',
                            'name' => 'libs[map][bounding_box][ne]',
                            'value' => isset($settings['libs.map.bounding_box']['ne']) ? $settings['libs.map.bounding_box']['ne'] : ''
                        ),
                        'bounding_box.sw' => array(
                            'class' => 'keymap-value',
                            'type' => 'text',
                            'name' => 'libs[map][bounding_box][sw]',
                            'value' => isset($settings['libs.map.bounding_box']['sw']) ? $settings['libs.map.bounding_box']['sw'] : ''
                        ),
                        'zoom' => array('type' => 'text', 'value' => isset($settings['libs.map.zoom']) ? $settings['libs.map.zoom'] : '')
                    ),
                ),
                'booking_form' => array(
                    'region' => array(
                        'enabled' => isset($settings['booking_form.region.enabled']) && $settings['booking_form.region.enabled'],
                        'values' => array(
                            'type' => 'selectize',
                            'selected' => join(',', $bookingFormRegionEnabledCollection),
                            'value' => json_encode($bookingFormRegionCollection)
                        )
                    )
                ),
                'search' => array(
                    'search.group' => array(
                        'noEnabler' => true,
                        'multiRoomSearch' => array(
                            'name' => 'search[multiRoomSearch][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['search.multiRoomSearch.enabled']) ? $settings['search.multiRoomSearch.enabled'] : ''
                        ),
                    ),
                    'threshold' => array(
                        'enabled' => isset($settings['search.threshold.enabled']) && $settings['search.threshold.enabled'],
                        'values' => array('type' => 'text', 'value' => isset($settings['search.threshold.values']) ? $settings['search.threshold.values'] : '')
                    ),
                    'alternatives' => array(
                        'enabled' => isset($settings['search.alternatives.enabled']) && $settings['search.alternatives.enabled'],
                        'duration' => array(
                            'type' => 'empty',
                        ),
                        'duration.enabled' => array(
                            'class' => 'keymap-value',
                            'type' => 'text',
                            'name' => 'search[alternatives][duration][enabled]',
                            'value' => isset($settings['search.alternatives.duration']['enabled']) ? $settings['search.alternatives.duration']['enabled'] : ''
                        ),
                        'duration.maxAlternativesShown' => array(
                            'class' => 'keymap-value',
                            'type' => 'text',
                            'name' => 'search[alternatives][duration][maxAlternativesShown]',
                            'value' => isset($settings['search.alternatives.duration']['maxAlternativesShown']) ? $settings['search.alternatives.duration']['maxAlternativesShown'] : ''
                        ),
                        'duration.durationFutureOffset' => array(
                            'class' => 'keymap-value',
                            'type' => 'text',
                            'name' => 'search[alternatives][duration][durationFutureOffset]',
                            'value' => isset($settings['search.alternatives.duration']['durationFutureOffset']) ? $settings['search.alternatives.duration']['durationFutureOffset'] : ''
                        ),
                        'duration.durationPastOffset' => array(
                            'class' => 'keymap-value',
                            'type' => 'text',
                            'name' => 'search[alternatives][duration][durationPastOffset]',
                            'value' => isset($settings['search.alternatives.duration']['durationPastOffset']) ? $settings['search.alternatives.duration']['durationPastOffset'] : ''
                        ),
                        'noFilter' => array(
                            'type' => 'empty',
                        ),
                        'noFilter.durationPastOffset' => array(
                            'class' => 'keymap-value',
                            'type' => 'text',
                            'name' => 'search[alternatives][noFilter][enabled]',
                            'value' => isset($settings['search.alternatives.noFilter']['enabled']) ? $settings['search.alternatives.noFilter']['enabled'] : ''
                        ),
                        'noFilter.maxAlternativesShown' => array(
                            'class' => 'keymap-value',
                            'type' => 'text',
                            'name' => 'search[alternatives][noFilter][maxAlternativesShown]',
                            'value' => isset($settings['search.alternatives.noFilter']['maxAlternativesShown']) ? $settings['search.alternatives.noFilter']['maxAlternativesShown'] : ''
                        ),
                    )
                ),
                'sort' => array(
                    'sort.group' => array(
                        'noEnabler' => true,
                        'unsortedEnabled' => array(
                            'name' => 'sort[unsortedEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['sort.unsortedEnabled.enabled']) && $settings['sort.unsortedEnabled.enabled']
                        ),
                        'relevanceEnabled' => array(
                            'name' => 'sort[relevanceEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['sort.relevanceEnabled.enabled']) && $settings['sort.relevanceEnabled.enabled']
                        ),
                        'orderByMinPriceIfStarOrdering' => array(
                            'name' => 'sort[orderByMinPriceIfStarOrdering][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['sort.orderByMinPriceIfStarOrdering.enabled']) && $settings['sort.orderByMinPriceIfStarOrdering.enabled']
                        ),
                    ),
                    'endpoints' => array(
                        'enabled' => isset($settings['sort.endpoints.enabled']) && $settings['sort.endpoints.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $endPointCollection
                        )
                    ),
                    'default' => array(
                        'enabled' => isset($settings['sort.default.enabled']) && $settings['sort.default.enabled'],
                        'values' => array(
                            'type' => 'radios',
                            'value' => $sort
                        )
                    ),
                    'order' => array(
                        'enabled' => isset($settings['sort.order.enabled']) && $settings['sort.order.enabled'],
                        'values' => array(
                            'type' => 'radios',
                            'value' => $order
                        )
                    ),
                ),
                'view' => array(
                    'view.group' => array(
                        'noEnabler' => true,
                        'contactBoxEnabled' => array(
                            'name' => 'view[contactBoxEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['view.contactBoxEnabled.enabled']) && $settings['view.contactBoxEnabled.enabled']
                        ),
                        'cartEnabled' => array(
                            'name' => 'view[cartEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['view.cartEnabled.enabled']) && $settings['view.cartEnabled.enabled']
                        ),
                        'cartAutoCheckoutButtonEnabled' => array(
                            'name' => 'view[cartAutoCheckoutButtonEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['view.cartAutoCheckoutButtonEnabled.enabled']) && $settings['view.cartAutoCheckoutButtonEnabled.enabled']
                        ),
                        'shortageEnabled' => array(
                            'name' => 'view[shortageEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['view.shortageEnabled.enabled']) && $settings['view.shortageEnabled.enabled']
                        ),
                        'dtvEnabled' => array(
                            'name' => 'view[dtvEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['view.dtvEnabled.enabled']) && $settings['view.dtvEnabled.enabled']
                        ),
                        'ggvOnRequestButtonEnabled' => array(
                            'name' => 'view[ggvOnRequestButtonEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['view.ggvOnRequestButtonEnabled.enabled']) && $settings['view.ggvOnRequestButtonEnabled.enabled']
                        ),
                        'ggvBookingBoxEnabled' => array(
                            'name' => 'view[ggvBookingBoxEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['view.ggvBookingBoxEnabled.enabled']) && $settings['view.ggvBookingBoxEnabled.enabled']
                        ),
                        'climaCalculatorEnabled' => array(
                            'name' => 'view[climaCalculatorEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['view.climaCalculatorEnabled.enabled']) && $settings['view.climaCalculatorEnabled.enabled']
                        ),
                        'successsCustomTextEnabled' => array(
                            'name' => 'view[successsCustomTextEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['view.successsCustomTextEnabled.enabled']) && $settings['view.successsCustomTextEnabled.enabled']
                        ),
                        'gridLayoutEnabled' => array(
                            'name' => 'view[successsCustomTextEnabled][enabled]',
                            'type' => 'checkbox',
                            'value' => isset($settings['view.gridLayoutEnabled.enabled']) && $settings['view.gridLayoutEnabled.enabled']
                        ),
                    ),
                    'bookingInformation' => array(
                        'enabled' => isset($settings['view.bookingInformation.enabled']) && $settings['view.bookingInformation.enabled'],
                        'values' => array('type' => 'text', 'value' => isset($settings['view.bookingInformation.values']) ? $settings['view.bookingInformation.values'] : '')
                    ),
                    'winterImage' => array(
                        'enabled' => isset($settings['view.winterImage.enabled']) && $settings['view.winterImage.enabled'],
                        'startMonth' => array('type' => 'text', 'value' => isset($settings['view.winterImage.startMonth']) ? $settings['view.winterImage.startMonth'] : ''),
                        'endMonth' => array('type' => 'text', 'value' => isset($settings['view.winterImage.endMonth']) ? $settings['view.winterImage.endMonth'] : '')
                    ),
                    'bookingKit' => array(
                        'enabled' => isset($settings['view.bookingKit.enabled']) && $settings['view.bookingKit.enabled'],
                        'DOMname_formblatt' => array('type' => 'text', 'value' => isset($settings['view.bookingKit.DOMname_formblatt']) ? $settings['view.bookingKit.DOMname_formblatt'] : ''),
                        'geolocation' => array(
                            'type' => 'empty',
                        ),
                        'geolocation.ne' => array(
                            'class' => 'keymap-value',
                            'type' => 'text',
                            'name' => 'view[bookingKit][geolocation][ne]',
                            'value' => isset($settings['view.bookingKit.geolocation']['ne']) ? $settings['view.bookingKit.geolocation']['ne'] : ''
                        ),
                        'geolocation.sw' => array(
                            'class' => 'keymap-value',
                            'type' => 'text',
                            'name' => 'view[bookingKit][geolocation][sw]',
                            'value' => isset($settings['view.bookingKit.geolocation']['sw']) ? $settings['view.bookingKit.geolocation']['sw'] : ''
                        )
                    )
                ),
                //Kommen noch mehr filter dazu
                'filter' => array(
                    'price' => array(
                        'enabled' => isset($settings['filter.price.enabled']) && $settings['filter.price.enabled'],
                        'min' => array('type' => 'text',  'value' => isset($settings['filter.price.min']) ? $settings['filter.price.min'] : ''),
                        'max' => array('type' => 'text', 'value' => isset($settings['filter.price.max']) ? $settings['filter.price.max'] : ''),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.price.open']) && $settings['filter.price.open']
                        )
                    ),
                    'city' => array(
                        'enabled' => isset($settings['filter.city.enabled']) && $settings['filter.city.enabled'],
//                        'values' => array(
//                            'type' => 'checkboxes',
//                            'value' => $cities
//                        ),
                        'values' => array(
                            'type' => 'selectize',
                            'selected' => join(',', $selectedCities),
                            'value' => ''
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.city.open']) && $settings['filter.city.open']
                        )
                    ),
                    'description' => array(
                        'enabled' => isset($settings['filter.description.enabled']) && $settings['filter.description.enabled'],
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.description.open']) && $settings['filter.description.open']
                        )
                    ),
                    'idsearch' => array (
                        'enabled'  => isset($settings['filter.idsearch.enabled']) && $settings['filter.idsearch.enabled'],
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.idsearch.open']) && $settings['filter.idsearch.open']
                        )
                    ),
                    'fulltext' => array (
                        'enabled'  => isset($settings['filter.fulltext.enabled']) && $settings['filter.fulltext.enabled'],
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.fulltext.open']) && $settings['filter.fulltext.open']
                        )
                    ),
                    'oCategory' => array(
                        'enabled' => isset($settings['filter.oCategory.enabled']) && $settings['filter.oCategory.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.oCategory'], isset($settings['filter.oCategory.values']) ? $settings['filter.oCategory.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset ($settings['filter.oCategory.open']) && $settings['filter.oCategory.open']
                        )
                    ),
                    'oFacility' => array(
                        'enabled' => isset($settings['filter.oFacility.enabled']) && $settings['filter.oFacility.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.oFacility'], isset($settings['filter.oFacility.values']) ? $settings['filter.oFacility.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.oFacility.open']) && $settings['filter.oFacility.open']
                        )
                    ),
                    'rFacility' => array(
                        'enabled' => isset($settings['filter.rFacility.enabled']) && $settings['filter.rFacility.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.rFacility'], isset($settings['filter.rFacility.values']) ? $settings['filter.rFacility.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.rFacility.open']) && $settings['filter.rFacility.open']
                        )
                    ),
                    'oFacility2' => array(
                        'enabled' => isset($settings['filter.oFacility2.enabled']) && $settings['filter.oFacility2.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.oFacility2'], isset($settings['filter.oFacility2.values']) ? $settings['filter.oFacility2.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.oFacility2.open']) && $settings['filter.oFacility2.open']
                        )
                    ),
                    'sustainability' => array(
                        'enabled' => isset($settings['filter.sustainability.enabled']) && $settings['filter.sustainability.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.sustainability'], isset($settings['filter.sustainability.values']) ? $settings['filter.sustainability.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.sustainability.open']) && $settings['filter.sustainability.open']
                        )
                    ),
                    'beds' => array(
                        'enabled' => isset($settings['filter.beds.enabled']) && $settings['filter.beds.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.beds'], isset($settings['filter.beds.values']) ? $settings['filter.beds.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.beds.open']) && $settings['filter.beds.open']
                        )
                    ),
                    'minStar' => array(
                        'enabled' => isset($settings['filter.minStar.enabled']) && $settings['filter.minStar.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.minStar'], isset($settings['filter.minStar.values']) ? $settings['filter.minStar.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.minStar.open']) && $settings['filter.minStar.open']
                        )
                    ),
                    'ratingCount' => array(
                        'enabled' => isset($settings['filter.ratingCount.enabled']) && $settings['filter.ratingCount.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.ratingCount'], isset($settings['filter.ratingCount.values']) ? $settings['filter.ratingCount.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.ratingCount.open']) && $settings['filter.ratingCount.open']
                        )
                    ),
                    'distanceStrand' => array(
                        'enabled' => isset($settings['filter.distanceStrand.enabled']) && $settings['filter.distanceStrand.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceStrand'], isset($settings['filter.distanceStrand.values']) ? $settings['filter.distanceStrand.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceStrand.open']) && $settings['filter.distanceStrand.open']
                        )
                    ),
                    'distanceSee' => array(
                        'enabled' => isset($settings['filter.distanceSee.enabled']) && $settings['filter.distanceSee.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceSee'], isset($settings['filter.distanceSee.values']) ? $settings['filter.distanceSee.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceSee.open']) && $settings['filter.distanceSee.open']
                        )
                    ),
                    'distanceBadestelle' => array(
                        'enabled' => isset($settings['filter.distanceSee.enabled']) && $settings['filter.distanceSee.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceBadestelle'], isset($settings['filter.distanceBadestelle.values']) ? $settings['filter.distanceBadestelle.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceBadestelle.open']) && $settings['filter.distanceBadestelle.open']
                        )
                    ),
                    'distanceBahnhof' => array(
                        'enabled' => isset($settings['filter.distanceSee.enabled']) && $settings['filter.distanceSee.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceBahnhof'], isset($settings['filter.distanceBahnhof.values']) ? $settings['filter.distanceBahnhof.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceBahnhof.open']) && $settings['filter.distanceBahnhof.open']
                        )
                    ),
                    'distanceZentrum' => array(
                        'enabled' => isset($settings['filter.distanceSee.enabled']) && $settings['filter.distanceSee.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceZentrum'], isset($settings['filter.distanceZentrum.values']) ? $settings['filter.distanceZentrum.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceZentrum.open']) && $settings['filter.distanceZentrum.open']
                        )
                    ),
                    'distanceBus' => array(
                        'enabled' => isset($settings['filter.distanceSee.enabled']) && $settings['filter.distanceSee.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceBus'], isset($settings['filter.distanceBus.values']) ? $settings['filter.distanceBus.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceBus.open']) && $settings['filter.distanceBus.open']
                        )
                    ),
                    'distanceSkibus' => array(
                        'enabled' => isset($settings['filter.distanceSee.enabled']) && $settings['filter.distanceSee.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceSkibus'], isset($settings['filter.distanceSkibus.values']) ? $settings['filter.distanceSkibus.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceSkibus.open']) && $settings['filter.distanceSkibus.open']
                        )
                    ),
                    'distanceSkilift' => array(
                        'enabled' => isset($settings['filter.distanceSkilift.enabled']) && $settings['filter.distanceSkilift.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceSkilift'], isset($settings['filter.distanceSkilift.values']) ? $settings['filter.distanceSkilift.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceSkilift.open']) && $settings['filter.distanceSkilift.open']
                        )
                    ),
                    'distanceSkigebiet' => array(
                        'enabled' => isset($settings['filter.distanceSkigebiet.enabled']) && $settings['filter.distanceSkigebiet.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceSkigebiet'], isset($settings['filter.distanceSkigebiet.values']) ? $settings['filter.distanceSkigebiet.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceSkigebiet.open']) && $settings['filter.distanceSkigebiet.open']
                        )
                    ),
                    'distanceLoipe' => array(
                        'enabled' => isset($settings['filter.distanceLoipe.enabled']) && $settings['filter.distanceLoipe.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceLoipe'], isset($settings['filter.distanceLoipe.values']) ? $settings['filter.distanceLoipe.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceLoipe.open']) && $settings['filter.distanceLoipe.open']
                        )
                    ),
                    'distanceTherme' => array(
                        'enabled' => isset($settings['filter.distanceTherme.enabled']) && $settings['filter.distanceTherme.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceTherme'], isset($settings['filter.distanceTherme.values']) ? $settings['filter.distanceTherme.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceTherme.open']) && $settings['filter.distanceTherme.open']
                        )
                    ),
                    'distanceTouristinfo' => array(
                        'enabled' => isset($settings['filter.distanceTouristinfo.enabled']) && $settings['filter.distanceTouristinfo.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceTouristinfo'], isset($settings['filter.distanceTouristinfo.values']) ? $settings['filter.distanceTouristinfo.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceTouristinfo.open']) && $settings['filter.distanceTouristinfo.open']
                        )
                    ),
                    'distanceWanderweg' => array(
                        'enabled' => isset($settings['filter.distanceWanderweg.enabled']) && $settings['filter.distanceWanderweg.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceWanderweg'], isset($settings['filter.distanceWanderweg.values']) ? $settings['filter.distanceWanderweg.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceWanderweg.open']) && $settings['filter.distanceWanderweg.open']
                        )
                    ),
                    'distanceRestaurant' => array(
                        'enabled' => isset($settings['filter.distanceRestaurant.enabled']) && $settings['filter.distanceRestaurant.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceRestaurant'], isset($settings['filter.distanceRestaurant.values']) ? $settings['filter.distanceRestaurant.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceRestaurant.open']) && $settings['filter.distanceRestaurant.open']
                        )
                    ),
                    'distanceRadweg' => array(
                        'enabled' => isset($settings['filter.distanceRadweg.enabled']) && $settings['filter.distanceRadweg.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.distanceRadweg'], isset($settings['filter.distanceRadweg.values']) ? $settings['filter.distanceRadweg.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.distanceRadweg.open']) && $settings['filter.distanceRadweg.open']
                        )
                    ),
                    'seal' => array (
                        'enabled'  => isset($settings['filter.seal.enabled']) && $settings['filter.seal.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.seal'], isset($settings['filter.seal.values']) ? $settings['filter.seal.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.seal.open']) && $settings['filter.seal.open']
                        )
                    ),
                    'leisure' => array (
                        'enabled'  => isset($settings['filter.leisure.enabled']) && $settings['filter.leisure.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.leisure'], isset($settings['filter.leisure.values']) ? $settings['filter.leisure.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.leisure.open']) && $settings['filter.leisure.open']
                        )
                    ),
                    'occupation' => array(
                        'enabled' => isset($settings['filter.occupation.enabled']) && $settings['filter.occupation.enabled'],
                        'min' => array('type' => 'text', 'value' => isset($settings['filter.occupation.min']) ? $settings['filter.occupation.min'] : ''),
                        'max' => array('type' => 'text', 'value' => isset($settings['filter.occupation.max']) ? $settings['filter.occupation.max'] : ''),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.occupation.open']) && $settings['filter.occupation.open']
                        )
                    ),
                    'offers' => array (
                        'enabled'  => isset($settings['filter.offers.enabled']) && $settings['filter.offers.enabled'],
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.offers.open']) && $settings['filter.offers.open']
                        )
                    ),
                    'bookType' => array (
                        'enabled'  => isset($settings['filter.bookType.enabled']) && $settings['filter.bookType.enabled'],
                        'values' => array(
                            'type' => 'checkboxes',
                            'value' => $this->generateListData($settings['system.filter.bookType'], isset($settings['filter.bookType.values']) ? $settings['filter.bookType.values'] : array())
                        ),
                        'open' => array(
                            'type' => 'checkbox',
                            'value' => isset($settings['filter.bookType.open']) && $settings['filter.bookType.open']
                        )
                    )
                ),
                'template' => array(
                    'template.group' => array(
                        'noEnabler' => true,
                        'list' => array(
                            'name' => 'template[list]',
                            'type' => 'text',
                            'value' => isset($template['list']) ? $template['list'] : ''
                        ),
                        'details' => array(
                            'name' => 'template[details]',
                            'type' => 'text',
                            'value' => isset($template['details']) ? $template['details'] : ''
                        ),
                        'note' => array(
                            'name' => 'template[note]',
                            'type' => 'text',
                            'value' => isset($template['note']) ? $template['note'] : ''
                        ),
                        'checkout' => array(
                            'name' => 'template[checkout]',
                            'type' => 'text',
                            'value' => isset($template['checkout']) ? $template['checkout'] : ''
                        ),
                    )
                )
            );

        } else {
            return false;
        }

        return $data;
    }

    public function generateListData($list, $matchList) {
        $oCategoryCollection = array();
        if(!is_array($matchList)) {
            $matchList = array();
        }
        foreach($list as $category) {
            if(is_array($category)) {
                $oCategoryCollection[] = array('value' => $category['value'], 'name' => $category['name'], 'enabled' =>  in_array($category['value'], $matchList));
            }
        }
        return $oCategoryCollection;
    }
}
