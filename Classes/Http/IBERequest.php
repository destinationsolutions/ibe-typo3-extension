<?php
/**
 * Created by PhpStorm.
 * User: michael.kirchner
 * Date: 10.05.19
 * Time: 16:39
 */

namespace Orcas\HrsIbe\Http;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class IBERequest {
    const IBE_SERVER_PROD         = 'https://www.dsibe.com';
    const IBE_SERVER_DEV          = 'https://dev.dsibe.com';
    const IBE_DEV_SERVER_USERNAME = 'ibe-dev';
    const IBE_DEV_SERVER_PASSWORD = 'ibe-dev';

    protected $urlPrefix = '';
    protected $apiRoute = '';
    protected $interfaceId = '';
    protected $requestType = '';
    protected $params = array();
    protected $rawParameters = array();
    protected $pageAutoDetection = true;
    protected $jsUrlPrefix = '';
    protected $authentication = '';
    protected $fieldPrefix = '';
    protected $singlePage = false;
    protected $useCustomConfiguration = true;

    protected $pluginPath = false;
    protected $apiKey = false;
    protected $useCurl = true;

    protected $forceType = array(
        'array' => array(
            'city','object_category', 'object_facility', 'room_facility', 'food_drink', 'sustainability', 'bed', 'star', 'rating',
            'distanceStrand', 'distanceSee', 'distanceBadestelle', 'distanceBahnhof', 'distanceZentrum', 'distanceBus',
            'distanceSkibus', 'distanceSkilift', 'distanceSkigebiet', 'distanceLoipe', 'distanceTherme', 'distanceTouristinfo',
            'distanceWanderweg', 'distanceRestaurant', 'distanceRadweg', 'oCategory', 'beds',
            'minStar', 'ratingCount', 'oFacility', 'rFacility', 'oFacility2'
        )
    );

    public function __construct($interfaceId, $requestType = 'GET')
    {
        $this->interfaceId = $interfaceId;
        $this->requestType = $requestType;

        $params = array();
        if(isset($_POST)) {
            $params = array_merge($params, $_POST);
        }

        if(isset($_GET)) {
            if(isset($_GET['order'])) {
                $_GET['ibe-order'] = $_GET['order'];
                unset($_GET['order']);
            }

            if(isset($_GET['sort'])) {
                $_GET['ibe-sort'] = $_GET['sort'];
                unset($_GET['sort']);
            }

            $params = array_merge($params, $_GET);
        }

        if($this->requestType == 'POST') {
            $this->params = $this->rawParameters = $params;
        } else {
            $this->params = $this->rawParameters = $params;
        }

        $this->params = $this->parseRequest();

        if(isset($this->params['interface_id'])) {
            unset($this->params['interface_id']);
        }
    }

    public function setApiKey($apiKey) {
        $this->apiKey = $apiKey;
    }

    public function setPluginPath($value) {
        $this->pluginPath = $value;
    }

    public function loadConfiguration() {
        if($this->apiKey) {
            $this->requestType = 'POST';
            $this->useCustomConfiguration = false;
            $this->addParam('api_key', $this->apiKey);
            return $this->send('load/external/configuration');
        }

        return false;
    }

    public function validateApiKey() {
        if($this->apiKey) {
            $this->requestType = 'POST';
            $this->useCustomConfiguration = false;
            $this->addParam('api_key', $this->apiKey);
            $response = $this->send('validate/api/key');
            if(is_array($response) && isset($response['valid']) && $response['valid']) {
                return true;
            }
        }

        return false;
    }

    public function saveConfiguration($data) {
        if($this->apiKey) {
            $this->requestType = 'POST';
            $this->useCustomConfiguration = false;
            $this->addParam('data', $data);
            $this->addParam('api_key', $this->apiKey);
            $this->send('save/external/configuration');
        }
    }

    private function parseRequest() {
        if(isset($this->params['route'])) {
            $this->requestType = 'GET';
            $query = explode("?", $this->params['route']);
            $params = array();

            if(isset($query[1])) {
                $data = explode("&", $query[1]);

                foreach ($data as $keySet) {
                    $raw = explode("=", $keySet);
                    $params[$raw[0]] = $raw[1];
                }
            }

            $this->rawParameters = $params;
            return $params;
        }

        return $this->params;
    }

    public function setSinglePage($value) {
        $this->singlePage = $value;
    }

    private function updateParameterKeys() {
        $this->params = array();
        $regex = "^" . $this->fieldPrefix . '-';
        foreach($this->rawParameters as $key => $value) {
            if(preg_match("/$regex/", $key) || $key == '_o') {
                $newKey = preg_replace("/$regex/", '', $key);
                $this->params[$newKey] = $value;
            }
        }
    }

    private function getWrapperParameterKeys() {
        $params = array();
        $regex = "^" . $this->fieldPrefix . '-';
        foreach($this->rawParameters as $key => $value) {
            if(!preg_match("/$regex/", $key)) {
                $newKey = preg_replace("/$regex/", '', $key);
                $params[$newKey] = $value;
            }
        }

        return $params;
    }

    public function setApiRoute($route) {
        $this->apiRoute = $route;
    }

    public function setFieldPrefix($prefix) {
        $this->fieldPrefix = $prefix;
        $this->updateParameterKeys();
    }

    public function setHttpAuthentication($user, $password) {
        if(strlen(trim($user)) > 0 && strlen(trim($password)) > 0) {
            $this->authentication = $user . ':' . $password;
        } else {
            $this->authentication = '';
        }
    }

    /**
     * Sets authentication values for dev api route usage.
     */
    public function setDevServerAuthentication() {
        // set authentication for dev server usage
        if ($this->apiRoute == self::IBE_SERVER_DEV) {
            $this->setHttpAuthentication(
                self::IBE_DEV_SERVER_USERNAME,
                self::IBE_DEV_SERVER_PASSWORD
            );
        }
    }

    /**
     * @param bool $pageAutoDetection
     * @return IBERequest
     */
    public function setPageAutoDetection($pageAutoDetection)
    {
        $this->pageAutoDetection = $pageAutoDetection;
        return $this;
    }

    /**
     * @param string $jsUrlPrefix
     */
    public function setJsUrlPrefix($jsUrlPrefix)
    {
        $this->jsUrlPrefix = $jsUrlPrefix;
        return $this;
    }

    /**
     * @param string $urlPrefix
     */
    public function setUrlPrefix($urlPrefix)
    {
        $this->urlPrefix = trim($urlPrefix);
        return $this;
    }

    public function getParams() {
        $params = $this->params;

        if(strlen($this->urlPrefix) > 0) $params['url_prefix'] = $this->urlPrefix;

        return $params;
    }

    public function addParam($key, $value, $overwrite = false) {
        //if the is required as array and is not an array than convert it to an array
        if(in_array($key, $this->forceType['array']) && !is_array($value)) {
            $value = array($value);
        }

        if(!isset($this->params[$key]) || $overwrite) {
            $this->params[$key] = $value;
        }
    }

    function checkHttps() {

        if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {

            return true;
        }
        return false;
    }

    protected function send($route, $getParams = array(), $debug = false) {
        $params = $this->getParams();

        if(strlen(trim($this->fieldPrefix)) > 0) {
            $params['field_prefix'] = $this->fieldPrefix;
            $params['field_js_prefix'] = $this->fieldPrefix;
        }

        if(strlen(trim($this->jsUrlPrefix)) > 0) {
            $params['url_js_prefix'] = $this->jsUrlPrefix;
        }

        if($this->singlePage) {
            $staticRoute = explode('?', $_SERVER['REQUEST_URI']);

            $params['static_route'] = $staticRoute[0];
            foreach ($this->getWrapperParameterKeys() as $key => $value) {
                $params['wrapper_' . $key] = $value;
            }
        }

        $scheme = $this->checkHttps() ? 'https' : 'http';

        //var_dump($_SERVER['REQUEST_SCHEME']); die;
        $params['external_host'] = $scheme . '://' . $_SERVER['HTTP_HOST'];

        $params['wrapper_api_call'] = true;
        $params['ignore_font'] = true;
        $params['use_custom_configuration'] = $this->useCustomConfiguration;

        if($this->pluginPath) {
            $params['plugin_path'] = $this->pluginPath;
        }

        if($debug) {
            var_dump($params);
        }

        $query = count($params) > 0 ? http_build_query($params) : '';

        if(count($getParams) > 0) {
            $getParams = '&' . http_build_query($getParams);
        } else {
            $getParams = '';
        }

        if($this->useCurl) {
            return $this->sendCurl($route, $getParams, $query);
        }

        return $this->sendNoCurl($route, $getParams, $query);
    }

    /**
     * Calls ibe filter counter routes and returns corresponding json response.
     *
     * Provided routes:
     * - filter/counter
     * --> Used to calc filter counter only for opened filter keys
     * --> Params: _o, openFilterKeys[], page ...
     *
     * - filter/countFilterGroup
     * --> Used to calc filter counter for a specific (selected) filterGroup
     * --> Params: _o, filterGroup, page ...
     *
     * Info:
     * Param interface_id will set on sendCurl() or sendNoCurl() method,
     * therefore it's ignored here.
     *
     * @param $route
     *
     * @return bool|mixed|string JSON response of called ibe api route
     */
    protected function sendCounter($route)
    {
        $this->rawParameters['use_custom_configuration'] = $this->useCustomConfiguration;
        $query = '';

        foreach ($this->rawParameters as $key => $value) {
            if ($key == 'openFilterKeys') {
                foreach ($value as $value1) {
                    $query .= '&' . $key . '[]=' . $value1;
                }
            } else if (!in_array($key, ['interface_id', 'ibe-action', 'use-custom-configuration'])) {
                $query .= '&' . $key . '=' . $value;
            } else {
                // do nothing
            }
        }

        // Remove first '&' sign because '&' is already set on sendCurl() or sendNoCurl() method.
        $query = substr($query, 1);

        if ($this->useCurl) {
            return $this->sendCurl($route, [], $query);
        }

        return $this->sendNoCurl($route, [], $query);
    }

    /**
     * Calls the expedia booking routes.
     *
     * @param $route
     *
     * Provided routes:
     * - ibeObject/expedia/book/verification
     * --> Used to verificate responsed payment state
     * --> Params: session_id
     *
     * - ibeObject/expedia/book/resetState
     * --> Used to remove buchungsnr, bookingState session data from SessionService
     * --> Params: session_
     *
     * - ibeObject/expedia/book/mail
     * --> Used to send confirmation mail after successfully payment response.
     * --> Params: bookingData (Array), buchungsnr
     *
     * Info:
     * Param interface_id will set on sendCurl() or sendNoCurl() method,
     * therefore it's ignored here.
     *
     * @return bool|mixed|string
     */
    private function sendExpediaData($route) {
        $array_filter = [];
        foreach ($this->rawParameters as $key => $item) {
            if (in_array($key, ['session_id', 'buchungsnr', 'bookingData'])) {
                $array_filter[$key] = $item;
            }
        }
        $fields = $array_filter;

        // Remove first '&' sign because '&' is already set on sendCurl() or sendNoCurl() method.
        $query = http_build_query($fields);

        if ($this->useCurl) {
            return $this->sendCurl($route, '', $query);
        }

        return $this->sendNoCurl($route, '', $query);
    }

    private function sendCurl($route, $getParams, $query) {
        if($this->requestType == 'POST') {
            $ch = \curl_init($this->apiRoute . '/api/v1/' . "$route?interface_id=" . $this->interfaceId . $getParams);
            \curl_setopt($ch, CURLOPT_POST, 1);
            \curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        } else {
            $ch = curl_init($this->apiRoute . '/api/v1/' . "$route?interface_id=" . $this->interfaceId . '&' . $query);
        }

        $headers = array(
            "X-IBE-TYPO3: " . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getExtensionVersion('orcas/hrs-ibe'),
            "user-agent: " . $_SERVER['HTTP_USER_AGENT']
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        \curl_setopt($ch, CURLOPT_COOKIE, 'XDEBUG_SESSION=PHPSTORM');
        if(strlen($this->authentication) > 0) {
            \curl_setopt($ch, CURLOPT_USERPWD, $this->authentication);
        }

        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        \curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        \curl_close($ch);

        $jsonResponse = json_decode($output, true);
        if(json_last_error() == JSON_ERROR_NONE) {
            return $jsonResponse;
        }

        return $output;
    }

    private function sendNoCurl($route, $getParams, $query) {
        $url = '';
        if($this->requestType == 'POST') {
            $url = $this->apiRoute . '/api/v1/' . "$route?interface_id=" . $this->interfaceId . $getParams;
        } else {
            $url = $this->apiRoute . '/api/v1/' . "$route?interface_id=" . $this->interfaceId . '&' . $query;
        }

        $protocol = 'http';
        if(strpos($this->apiRoute, 'https://') == 0) {
            $protocol = 'https';
            //$url = str_replace('https://', 'http://' . $this->authentication . '@', $url);
        }

        $auth = '';
        if(strlen($this->authentication) > 0) {
            $basicAuth = base64_encode($this->authentication);
            $auth = "\r\nAuthorization: Basic $basicAuth\r\n";
        }

        //$url = 'https://ibe.ds-destinationsolutions.com/?interface_id=ferien-auf-foehr';
        $opts = array('http' =>
            array(
                'method'  => $this->requestType,
                'header'  => "Content-Type: application/x-www-form-urlencoded$auth",
                'content' => $query
            )
        );

        $context = stream_context_create($opts);

        $fp = fopen($url, 'rb', false, $context);

        $output = stream_get_contents($fp);
        fclose($fp);
        //$output = file_get_contents($url, false, $context);
        //var_dump($query, $this->requestType, $this->authentication, $basicAuth, $url, $output);
        $jsonResponse = json_decode($output, true);

        if(json_last_error() == JSON_ERROR_NONE) {
            return $jsonResponse;
        }

        return $output;
    }

    private function getParam($param ) {
        $key = strlen($this->fieldPrefix) > 0 ? $this->fieldPrefix . '-' . $param : $param;
        if(isset($this->params[$param])) {
            return $this->params[$param];
        } else if(isset($this->rawParameters[$key])) {
            return $this->rawParameters[$key];
        }

        return false;
    }

    public function isResource() {
        return strpos($_SERVER['REDIRECT_URL'], '.woff') > 0 ||
            strpos($_SERVER['REDIRECT_URL'], '.svg') > 0 ||
            strpos($_SERVER['REDIRECT_URL'], '.ttf') > 0 ||
            strpos($_SERVER['REDIRECT_URL'], '.eot') > 0;
    }

    public function getPage() {
        if($this->pageAutoDetection) {
            if($this->isResource()) {
                echo file_get_contents(__DIR__ . '/resources/' . $_SERVER['REDIRECT_URL']);
            } else if(isset($this->params['action'])) {
                $action = $this->params['action'];
                unset($this->params['action']);
                $this->ajax($action);
            } else if(isset($_GET['action']) && $_GET['action'] == 'requestFunnel') {
                return $this->removeIbePageHeaderHtml($this->send('ibeObject/show/checkout/request'));
            } else if($this->getParam('cart')) {
                $this->requestType = 'POST';
                return $this->removeIbePageHeaderHtml($this->send('ibeObject/show/checkout/render'));
            } else if ($this->getParam('bookingtype')) {
                return $this->removeIbePageHeaderHtml($this->send('ibeObject/show/checkout/render'));
            } else if($this->getParam('ids')) {
                return $this->removeIbePageHeaderHtml($this->send('merkliste/render'));
            } else if($this->getParam('id')) {
                return $this->removeIbePageHeaderHtml($this->send('ibeObject/details/render'));
            } else {
                return $this->removeIbePageHeaderHtml($this->send('ibeObject/list/render'));
            }
        } else {
            return '';
        }
    }

    private function removeIbePageHeaderHtml($string) {
        return $string;
    }

    public function getPageAsJson() {
        if($this->pageAutoDetection) {
            if($this->getParam('room') || $this->getParam('cart')) {
                $this->requestType = 'POST';
                return $this->send('ibeObject/show/checkout');
            } else if($this->getParam('ids')) {
                return $this->send('ibeObject/noteList');
            }else if($this->getParam('id')) {
                return $this->send('ibeObject/' . $this->getParam('id'));
            } else {
                return $this->send('ibeObject/list');
            }
        } else {
            return '';
        }
    }

    public function getCustomPageKey() {
        if($this->pageAutoDetection) {
            if($this->getParam('room') || $this->getParam('cart')) {
                return 'checkout';
            } else if($this->getParam('ids')) {
                return 'note';
            }else if($this->getParam('id')) {
                return 'details';
            } else {
                return 'list';
            }
        }

        return false;
    }

    public function ajax($action) {
        $result = '';
        switch($action) {
            case 'counter':
                $result = $this->sendCounter('filter/counter');
                break;
            case 'countFilterGroup':
                $result = $this->sendCounter('filter/countFilterGroup');
                break;
            case 'object-rooms':
                $result = $this->send('ibeObject/' . $this->getParam('id') . '/rooms');
                break;
            case 'availability':
                $result = $this->send('availability/' . $this->getParam('id'), array());
                break;
            case 'cart':
                $this->requestType = 'POST';
                $result = $this->send('cart');
                break;
            case 'funnel':
            case 'buy':
                header('Content-Type: application/json');
                $this->requestType = 'POST';
                $result = $this->send('book/submit');
                break;
            case 'success':
                $result = $this->send('book/success');
                break;
            case 'msp-region':
                $result = $this->send('msp/region');
                break;
            case 'expedia-booking-verification':
                header('Content-Type: application/json');
                $this->requestType = 'POST';
                $result = $this->sendExpediaData('ibeObject/expedia/book/verification');
                break;
            case 'expedia-reset-booking-state':
                header('Content-Type: application/json');
                $this->requestType = 'POST';
                $result = $this->sendExpediaData('ibeObject/expedia/book/resetState');
                break;
            case 'expedia-confirmation-mail':
                header('Content-Type: application/json');
                $this->requestType = 'POST';
                $result = $this->sendExpediaData('ibeObject/expedia/book/mail');

                break;
            case 'list-marker':
                header('Content-Type: application/json');
                $result = $this->send('ibeObject/list/marker');
                break;
            case 'object-marker':
                header('Content-Type: application/json');
                $result = $this->send('ibeObject/' . $this->getParam('id') . '/marker');
                break;
            default:
                break;
        }

        if(is_array($result)) {
            $result = json_encode($result);
        }

        echo $result;
        die();
    }

    public function getHelp() {
        return $this->send('doc');
    }
}
