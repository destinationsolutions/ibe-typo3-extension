<?php
namespace Orcas\HrsIbe\Domain\Model;

use TYPO3\CMS\Extbase\Annotation as Extbase;

/***
 *
 * This file is part of the "HRS-IBE" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Kreetz <alexander.kreetz@orcas.de>, orcas
 *
 ***/

/**
 * IbeSettings
 */
class IbeSettings extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * settingKey
     *
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $settingKey = '';

    /**
     * value
     *
     * @var string
     * @Extbase\Validate("NotEmpty")
     */
    protected $value = '';

    /**
     * Returns the settingKey
     *
     * @return string $settingKey
     */
    public function getSettingKey()
    {
        return $this->settingKey;
    }

    /**
     * Sets the settingKey
     *
     * @param string $settingKey
     * @return void
     */
    public function setSettingKey($settingKey)
    {
        $this->settingKey = $settingKey;
    }

    /**
     * Returns the value
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value === null ? 0 : $this->value;
    }

    /**
     * Sets the value
     *
     * @param string $value
     * @return void
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
