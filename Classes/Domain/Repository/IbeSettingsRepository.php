<?php
namespace Orcas\HrsIbe\Domain\Repository;

/***
 *
 * This file is part of the "HRS-IBE" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Kreetz <alexander.kreetz@orcas.de>, orcas
 *
 ***/

/**
 * The repository for IbeSettings
 */
class IbeSettingsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @param $key
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByKey($key)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(false);
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->matching($query->equals('settingKey', $key));

        return $query->execute();
    }
}
